// function availableItems(array) {
//   const newArray = array.filter((item) => {
//     if (item.available) {
//       return item;
//     }
//     /*  In the if statement if the item.available returns true
//     then we return the item.   */
//   });
//   return newArray;
// }

/*
 The above method uses filter-method to search 
for items which are available 
*/

function availableItems(array) {
  const newArray = array.reduce((accumulator, currentValue) => {
    if (currentValue.available) {
      accumulator.push(currentValue);
    }
    return accumulator;
  }, []);
  /* We pass the empty array as the default and if we found the 
  current value to be true in the if-condition then we push the 
  value in the accumulator array  */
  return newArray;
}

module.exports = availableItems;
