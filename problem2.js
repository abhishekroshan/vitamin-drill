// function containOnlyVitaminC(array) {
//   const newArray = array.filter((item) => {
//     if (item.contains === "Vitamin C") {
//       return item;
//     }
//     /*
//     Since we only have to return with only Vitamin-C in it
//     we can perform a filter method  to check the contain
//     is equal to "Vitamin-C "
//     and then simply return the item if the if-conditon is true
//     */
//   });
//   return newArray;
// }

/* The Above Uses filter-method */

function containOnlyVitaminC(array) {
  const newArray = array.reduce((accumulator, currentValue) => {
    if (currentValue.contains === "Vitamin C") {
      accumulator.push(currentValue);
    }
    return accumulator;
  }, []);
  //initialized accumulator as an empty array
  return newArray;
}

/* this is the same implementation but with using reduce-method */

module.exports = containOnlyVitaminC;
