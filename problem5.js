function sortedOrder(array) {
  const result = array.sort((item1, item2) => {
    const vitamin1 = item1.contains.split(",").length;

    const vitamin2 = item2.contains.split(",").length;
    /*  
    calculating the length of the 
    contains by splitting it from the ","  
    */

    const answer = vitamin1 - vitamin2;
    //comparing the length of the item1 and item2

    return answer;
  });
  return result;
}

module.exports = sortedOrder;
