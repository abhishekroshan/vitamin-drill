function newObject(array) {
  const result = array.reduce((accumulator, currentValue) => {
    //initialized accumulator as an empty object
    const vitamins = currentValue.contains
      .split(",")
      .map((vitamin) => vitamin.trim());
    /* Splitting the contains into an array 
         and removing extra-spaces 
         */

    vitamins.forEach((element) => {
      if (!accumulator[element]) {
        accumulator[element] = [];
      }
      /*if the name is not present in the object we 
      create it with an empty array 
      */

      accumulator[element].push(currentValue.name);
      //push the element into the corrosponding array.
    });
    return accumulator;
  }, {});

  return result;
}

module.exports = newObject;
