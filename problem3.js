// function containVitaminA(array) {
//   const newArray = array.reduce((accumulator, currentValue) => {
//     if (currentValue["contains"].includes("Vitamin A")) {
//       //the .includes checks in the contains for the 'vitamin-1'
//       accumulator.push(currentValue);
//       /*  if the contain includes the 'vitamin-a' we push the
//       currentValue to the array.  */
//     }
//     return accumulator;
//   }, []);
//   return newArray;
// }

/*
the above is the implementation using the reduce method
*/

function containVitaminA(array) {
  const newArray = array.filter((value) => {
    if (value["contains"].includes("Vitamin A")) {
      //the .includes checks in the contains for the 'vitamin-1'
      return value;
    }
  });
  return newArray;
}

module.exports = containVitaminA;
